var fs = require ("fs");
var path = require ("path");
var webpack = require ("webpack");

var babelrc = JSON.parse (fs.readFileSync ('./.babelrc'));

/*
var plugins = [
    new webpack.optimize.CommonsChunkPlugin ("vendor", "vendor.js", (module) => {
        // If the module is in the `node_modules/` path, lets put it in the vendor.js file.
        return module.resource && module.resource.indexOf("node_modules") > -1;
    }),
    new webpack.IgnorePlugin (/^\.\/locale$/, /moment$/)
];
*/

module.exports = {
    entry: {
        app: ["main"]
    },
    output: {
        path: path.resolve (__dirname, "js"),
        publicPath: "/js/",
        filename: "main.js"
    },
    resolve: {
        root: [__dirname + "/src"],
        // you can now require("file") instead of require("file.js")
        extensions: ["", ".jsx", ".js", ".json"]
    },
    module: {
        preLoaders: [
            {
                test: /\.(js|jsx)$/,
                loader: "eslint-loader",
                include: __dirname + "/src"
            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel",
                query: babelrc
            },
            {
                include: /\.json$/,
                loaders: ["json-loader"]
            }
        ]
    }
};
