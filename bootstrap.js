/**
 *   This file is part of wald:grid.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

var _common_js = {
    moduleLoader: 'curl/loader/cjsm11'
};

var _paths = {
    'httpinvoke':         '../node_modules/httpinvoke/httpinvoke-browser.js',
    'jsonld':             '../node_modules/jsonld/js/jsonld.js',
    'react':              '../node_modules/react/dist/react-with-addons.js',
    'react-dom':          '../node_modules/react-dom/dist/react-dom.js',
    'rx':                 '../node_modules/rx/dist/rx.all.js',
    'underscore':         '../node_modules/underscore/underscore.js',
    'underscore.string':  '../node_modules/underscore.string/dist/underscore.string.js',
    'when':               '../node_modules/when/dist/browser/when.js',
};

var _packages = {
    'bcp-47': {
        location: '../node_modules/bcp-47/',
        main: 'index.js',
        config: _common_js,
    },
    'n3': {
        location: '../node_modules/n3/',
        main: 'browser/n3-browser.js',
        config: _common_js,
    },
    'urijs': {
        location: '../node_modules/urijs/src/',
        main: 'URI.js',
    },
    'wer': {
        location: '../node_modules/wer/lib/',
        main: 'wer.js',
    },
    'wald-view': {
        location: 'view/',
        main: 'view.js',
    },
};

var _config = {
    baseUrl: '/js/',
    paths: _paths,
    packages: _packages
};

curl(_config, ['main']).then(function () {
    console.log('application loaded');
}, function (err) {
    console.log('ERROR: ', err);
});
