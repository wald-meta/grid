/**
 *   This file is part of wald:grid.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

const bcp47 = require ('bcp-47');
const N3 = require ('n3');
const React = require ('react');
const ReactDOM = require ('react-dom');
const find = require ('wald-find');

const a = find.namespaces.rdf.type;
// const wm = find.namespaces.wm;
const rdf = find.namespaces.rdf;
const rdfs = find.namespaces.rdfs;
const owl = find.namespaces.owl;

const REQUIRED_MARK = <span className="fieldRequired">*</span>;
const LANGUAGE_TAG_WIDTH = 80;
const ADD_REMOVE_WIDTH = 40;
const WALD_GRID_COLUMN_WIDTH = 160;

function wald_grid_field_label (form, field) {
    var required = '';

    const isRequired = form.allObjects (field.predicate, rdfs.range)
          .filter ((range) => form.all (range, a, owl.Restriction).length)
          .map ((range) => find.tools.integer (form.firstObject (range, owl.minCardinality)))
          .filter ((item) => item).length > 0;

    if (isRequired) {
        required = REQUIRED_MARK;
    }

    return <span>{find.qname (field.predicate)} {required}</span>;
};

class TextComponent extends React.Component {
    render () {
        var field = this.props.field;
        var name = wald_grid_field_label (this.props.form, field);
        var style = { width: this.props.width };

        const value = N3.Util.getLiteralValue (field.value);

        return (
                <div className="wald-grid-field-component" style={style}>
                <label className="wald-grid-field-inputs">{name}</label>
                <input className="wald-grid-field-inputs value" type="text"
            defaultValue={value} onBlur={this.props.onBlur} />
                </div>
        )
    }
};
TextComponent.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    onBlur: React.PropTypes.func.isRequired,
    width: React.PropTypes.number.isRequired
};


class LinkComponent extends React.Component {
    render () {
        var field = this.props.field;
        var name = wald_grid_field_label (this.props.form, field);
        var style = { width: this.props.width };
        var href = field.value;

        return (
                <div className="wald-grid-field-component" style={style}>
                <label className="wald-grid-field-inputs">
                {name}
                <a className="wald-grid-field-link" href={href}>link</a>
                </label>
                <input className="wald-grid-field-inputs value" type="text"
            defaultValue={href} onBlur={this.props.onBlur} />
                </div>
        )
    }
};
LinkComponent.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    onBlur: React.PropTypes.func.isRequired,
    width: React.PropTypes.number.isRequired
};


class ImageComponent extends React.Component {
    render () {
        var field = this.props.field;

        // FIXME: move to CSS file?
        var imageStyle = { width: this.props.width, textAlign: 'right' };
        var imgStyle = { maxWidth: this.props.width };

        return (
                <div className="wald-grid-field-component" style={imageStyle}>
                <img className="wald-grid-field-image-component"
            src={field.value} style={imgStyle} />
                </div>
        );
    }
};
ImageComponent.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    width: React.PropTypes.number.isRequired
};


class LanguageComponent extends React.Component {
    render () {
        var field = this.props.field;
        var style = { width: this.props.width };

        let language = N3.Util.getLiteralLanguage (field.value);
        const parsedLang = bcp47.parse (language);
        parsedLang.region = parsedLang.region.toUpperCase ();
        language = bcp47.stringify (parsedLang);

        return (
                <div className="wald-grid-field-component" style={style}>
                <label className="wald-grid-field-inputs">language</label>
                <input className="wald-grid-field-inputs language"
            type="text" defaultValue={language} />
                </div>
        )
    }
};
LanguageComponent.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    width: React.PropTypes.number.isRequired
};


class AddRemoveComponent extends React.Component {
    render () {
        var style = { width: this.props.width };

        return (
                <div className="wald-grid-field-add-remove" style={style}>
                <button>&times;</button>
                <button>+</button>
                </div>
        )
    }
};
AddRemoveComponent.propTypes = {
    width: React.PropTypes.number.isRequired
};


class Literal extends React.Component {
    constructor (props) {
        super (props);
        this.state = {};

        this.handleBlur = this.handleBlur.bind (this);
    }

    addComponents (field, width) {
        const form = this.props.form;
        const components = [];

        // Language component
        if (field.range === rdfs.Literal || field.range === rdf.langString) {
            width -= LANGUAGE_TAG_WIDTH;
            components.push (
                    <LanguageComponent
                key="lang"
                field={field}
                form={this.props.form}
                width={LANGUAGE_TAG_WIDTH}
                    />
            );
        }

        // Add/Remove component
        const results = form.allObjects (field.predicate, rdfs.range)
              .filter ((range) => form.all (range, a, owl.Restriction).length)
              .map ((range) => find.tools.integer (
                  form.firstObject (range, owl.maxCardinality)));

        const maxCardinality = results.length > 0 ? results[0] : null;

        if (maxCardinality && maxCardinality === 1) {
            // only one value allowed for this field
        } else {
            // multiple values allowed for this field
            width -= ADD_REMOVE_WIDTH;
            components.push (
                    <AddRemoveComponent
                field={field}
                key="add-remove"
                width={ADD_REMOVE_WIDTH}
                    />
            );
        }

        return {
            before: [],
            after: components,
            remainingWidth: width
        };
    }

    handleBlur (event) {
    }

    mainComponent (width) {
        return (
                <TextComponent
            {...this.props}
            onBlur={this.handleBlur}
            width={width}
                />
        );
    }

    render () {
        var field = this.props.field;
        //            var name = wald_grid_field_label (field);
        var outerStyle = { width: WALD_GRID_COLUMN_WIDTH * field.size };
        var valueWidth = outerStyle.width;

        if (this.props.pos === 0) {
            valueWidth = outerStyle.width;
        } else {
            valueWidth = outerStyle.width - 1;
        }

        var extra = this.addComponents (field, valueWidth);
        const value = this.mainComponent (extra.remainingWidth);

        return (
                <div className="wald-grid-field" style={outerStyle}>
                {extra.before}
            {value}
            {extra.after}
            </div>
        );
    }
};
Literal.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    pos: React.PropTypes.number.isRequired,
};


class Link extends Literal {
    mainComponent (width) {
        return (
                <LinkComponent
            {...this.props}
            onBlur={this.handleBlur}
            width={width}
                />
        );
    }
};


class Image extends Link {

    addComponents (field, width) {
        var extra = super.addComponents (field, width);

        if (field.size < 2) {
            console.log ('WARNING: Image elements need a size of 2 or more', field);
            return extra;
        }

        var w = WALD_GRID_COLUMN_WIDTH;
        extra.before.push (
                <ImageComponent
            field={field}
            form={this.props.form}
            key="image"
            ref="preview"
            width={w}
                />
        );
        extra.remainingWidth -= w;

        return extra;
    }

    handleBlur (event) {
        // Update image preview when the URL to the image is edited
        // FIXME: update the value properly and get rid of this HTML manipulation
        var imgs = ReactDOM.findDOMNode (this.refs.preview).getElementsByTagName ('img');
        imgs[0].src = event.target.value;
    }
}
Image.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    field: React.PropTypes.object.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
};

module.exports = {
    'http://xmlns.com/foaf/0.1/Image': Image,
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#langString': Literal,
    'http://www.w3.org/2000/01/rdf-schema#Literal': Literal,
    'http://www.w3.org/2000/01/rdf-schema#Resource': Link,
    'http://www.w3.org/2001/XMLSchema#string': Literal,
};

