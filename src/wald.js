/**
 *   This file is part of wald:grid.
 *   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

var view = require ('wald-view');

module.exports = {
    version: "0.5.8",
    view: view
};


