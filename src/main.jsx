/**
 *   This file is part of wald:grid.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

const elements = require ('elements');
//    const fields = require ('fields');
const N3 = require ('n3');
const React = require ('react');
const ReactDOM = require ('react-dom');
const Rx = require ('rx');
const underscore = require ('underscore');
const find = require ('wald-find');

const a = find.namespaces.rdf.type;
const wm = find.namespaces.wm;
const rdf = find.namespaces.rdf;
const rdfs = find.namespaces.rdfs;
const owl = find.namespaces.owl;

const MAX_ROW_SIZE = 6;
const WALD_GRID_COLUMN_WIDTH = 160;

class FormRow extends React.Component {

    render () {
        const style = { width: 6 * WALD_GRID_COLUMN_WIDTH };
        const form = this.props.form;

        return (
                <div className="wald-grid-row" style={style}>
                {this.props.fields.map ((item, idx) => {
                    const key = 'field:' + idx + ':' + item.id;
                    if (elements.hasOwnProperty (item.range)) {
                        const Component = elements[item.range];
                        return (
                                <Component
                            data={this.props.data}
                            field={item}
                            form={form}
                            key={key}
                            pos={idx}
                                />
                        );
                    } else {
                        console.log ('ERROR: no component found for',
                                     item.id, find.qname (type));
                        return null;
                    }
                })}
            </div>
        );
    }
}
FormRow.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    fields: React.PropTypes.array.isRequired,
    form: React.PropTypes.instanceOf (find.Query).isRequired,
};

class FormGrid extends React.Component {
    constructor (props) {
        super (props);
        this.state = { required: false, rows: [] };
    }

    componentDidMount () {
        this._processForm (this.props);
    }

    componentWillReceiveProps (nextProps) {
        this._processForm (nextProps);
    }

    _processForm (props) {
        const form = props.form;
        const formId = form.firstSubject (a, wm.Form);
        const fieldIds = Rx.Observable.from (
            form.list (form.allObjects (formId, wm.fields)));

        // Required indicator
        fieldIds
            .map ((field) => form.firstObject (field, wm.predicate))
            .flatMap ((predicate) => form.allObjects (predicate, rdfs.range))
            .filter ((range) => form.firstObject (range, a, owl.Restriction) !== rdf.nil)
            .map ((restriction) => form.firstObject (restriction, owl.minCardinality))
            .map ((cardinality) => find.tools.integer (cardinality))
            .filter ((required) => required)
            .isEmpty ()
            .subscribe ((notRequired) => this.setState ({ required: !notRequired }));

        // Determine the layout of the form
        this._allocateRows (props, fieldIds);
    }

    _allocateRows (props, fieldIds) {
        const form = props.form;

        fieldIds
            .flatMap ((fieldId) => {
                const predicate = form.firstObject (fieldId, wm.predicate);
                let values = [];

                if (props.data) {
                    values = props.data.allObjects (props.subject, predicate);
                }

                if (!values.length) {
                    values = [ null ];
                }

                const ranges = underscore (form.allObjects (predicate, rdfs.range))
                      .filter ((range) => {
                          return N3.Util.isIRI (range) &&
                              !form.all (range, a, owl.Restriction).length;
                      });

                const range = ranges.length ? ranges[0] : null;

                return values.map (value => {
                    return {
                        id: fieldId,
                        predicate: predicate,
                        range: range,
                        value: value
                    };
                });
            })
            .map ((field) => {
                const minSize = find.tools.integer (form.firstObject (field.id, wm.minSize));

                field.minSize = minSize ? minSize : 1;
                field.size = field.minSize;
                field.stretchSize = find.tools.integer (
                    form.firstObject (field, wm.stretchSize));

                return field;
            })
            .scan ((row, field) => {
                const fieldSize = field.minSize ? field.minSize : 1;
                const rowSize = row.rowSize ? row.rowSize : fieldSize;
                const rowIdx = row.rowIdx ? row.rowIdx : 0;

                const newSize = rowSize + fieldSize;
                if (newSize > MAX_ROW_SIZE) {
                    field.rowSize = fieldSize;
                    field.rowIdx = rowIdx + 1;
                } else {
                    field.rowSize = newSize;
                    field.rowIdx = rowIdx;
                }
                return field;
            })
            .groupBy ((field) => field.rowIdx ? field.rowIdx : 0)
            .flatMap ((row) => row.toArray ().map ((fields) => this._fillRow (fields)))
            .toArray ()
            .subscribe ((data) => this.setState ({ rows: data }));
    }

    _fillRow (row) {
        const currentSize = row.reduce ((memo, item) => memo + item.size, 0);

        let remaining = MAX_ROW_SIZE - currentSize;
        if (remaining <= 0) {
            return row;
        }

        let somethingChanged = true;
        while (somethingChanged && remaining) {
            somethingChanged = false;
            row.forEach (function (item, idx) {
                if (!remaining) {
                    return;
                }

                if (item.stretch && item.size < item.stretch) {
                    item.size += 1;
                    remaining -= 1;
                    somethingChanged = true;
                }
            });
        }

        while (remaining) {
            row.forEach (function (item, idx) {
                if (!remaining) {
                    return;
                }

                item.size += 1;
                remaining -= 1;
            });
        }

        return row;
    }

    render () {
        let required = null;

        if (!this.props.form) {
            return null;
        }

        if (this.state.required) {
            required = <p>* = required</p>;
        }

        return (
                <div className="wald-grid">
                <fieldset className="wald-grid-fieldset">
                {this.state.rows.map ((item, idx) => {
                    const key = 'row:' + idx;

                    try {
                        return (
                                <FormRow
                            data={this.props.data}
                            fields={item}
                            form={this.props.form}
                            key={key}
                                />
                        );
                    } catch (e) {
                        console.log ('RENDER ERROR', e);
                        return <h1>ERROR</h1>;
                    }
                })}
            </fieldset>
                {required}
            </div>
        );
    }
};
FormGrid.propTypes = {
    data: React.PropTypes.instanceOf (find.Query),
    form: React.PropTypes.instanceOf (find.Query).isRequired,
    subject: React.PropTypes.string.isRequired,
};

class HelloWorld extends React.Component {
    constructor (props) {
        super (props);
        this.state = {form: null, data: null};
    }

    componentDidMount () {
        find.tools.loadTurtle ('/license-form.ttl').then (
            (datastore) => this.setState ({ form: datastore }),
            (err) => console.log ('error loading form', err)
        );

        find.tools.loadTurtle ('/copyleft-next-0.3.0.ttl').then (
            (datastore) => this.setState ({ data: datastore }),
            (err) => console.log ('error loading data', err)
        );
    }

    render () {
        var body = <p>Loading...</p>;

        if (this.state.form && this.state.data) {
            const form = find.factory (this.state.form);
            const data = this.state.data ? find.factory (this.state.data) : null;
            const subject = 'https://licensedb.org/id/copyleft-next-0.3.0';
            body = <FormGrid form={form} data={data} subject={subject} />;
        }

        return (
                <div>
                <h1>REACT test</h1>
                {body}
            </div>
        );
    }
};

ReactDOM.render (
        <HelloWorld />,
    document.getElementById ('main')
);


