/**
 *   This file is part of wald:grid.
 *   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

// datatypes for literals:
// - xsd:string,      Any string for which a language tag is not allowed
// - rdfs:Literal,    Any string for which a language tag is optional
// - rdf:langString,  Any string for which a language tag is required
// - rdf:HTML,        HTML content, should have language information embedded
// - sysont:Markdown, Markdown content, can have language information embedded?
// - rdfs:Resource,   A link, it is recommended the link can be resolved
//                    (though not required)
// - foaf:Image,      A link to an image, which needs to be resolvable

// cardinality:
// - owl:minCardinality 1, // required
// - owl:maxCardinality 1, // multiple values not allowed


// namespaces:
// sysont: http://ns.ontowiki.net/SysOnt/
// foaf: http://xmlns.com/foaf/0.1/
// wikidata: http://www.wikidata.org/entity/

var dc_title = {
    'dc:title': 'dc:title',
    value: 'GNU Affero General Public License',
    language: 'es-419',
    size: 2,
    stretch: 4,
    a: 'rdfs:Literal', // language tag optional
}

var dc_hasVersion = {
    'dc:title': 'dc:hasVersion',
    value: '3.0',
    size: 1,
    a: 'xsd:string', // language tag not allowed
    'owl:maxCardinality': 1, // multiple values not allowed
}

var li_id = {
    'dc:title': 'li:id',
    value: 'AGPLv3',
    size: 2,
    a: 'xsd:string', // language tag not allowed
    'owl:maxCardinality': 1, // multiple values not allowed
    'owl:minCardinality': 1, // this value is required
}

var foaf_logo = {
    'dc:title': 'foaf:logo',
    value: 'img/agplv3-155x51.png',
    // value: 'https://frob.nl/me.png',
    size: 3,
    stretch: 6,
    a: 'foaf:Image',
}

var dc_subject = {
    'dc:title': 'dc:subject',
    value: 'wikidata:Q1131681',
    size: 2,
    a: 'rdfs:Resource',
}

module.exports = {
    demo: [
        li_id,
        dc_title,
        dc_hasVersion,
        foaf_logo,
        dc_subject
    ]
};

